package data.api.parking;


import data.api.EcoBikeApi;
import data.api.IEcoBikeApi;
import org.junit.Before;
import org.junit.Test;
import java.util.HashMap;
import java.util.Map;
import static org.junit.Assert.assertNotNull;

public class SearchParkingWhiteTest {
    IEcoBikeApi api;

    @Before
    public void setUp() {
        api = EcoBikeApi.getInstance();
    }

    @Test
    public void searchParkingTest() {
        Map params = new HashMap();
        params.put("id", "1");
        params.put("name", "user123");
        params.put("address", "Hà Nội");
        assertNotNull(api.searchParking(params));
    }
}