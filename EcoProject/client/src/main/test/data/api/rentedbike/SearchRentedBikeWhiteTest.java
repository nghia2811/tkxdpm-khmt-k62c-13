package data.api.rentedbike;


import data.api.EcoBikeApi;
import data.api.IEcoBikeApi;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class SearchRentedBikeWhiteTest {
    IEcoBikeApi api;

    @Before
    public void setUp() {
        api = EcoBikeApi.getInstance();
    }

    @Test
    public void searchParkingTest() {
        Map params = new HashMap();
        params.put("parking_id", "1");
        params.put("username", "user123");
        params.put("card_number", "123");
        assertNotNull(api.searchRentedBike(params));
    }
}