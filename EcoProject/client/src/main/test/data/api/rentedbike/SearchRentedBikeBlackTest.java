package data.api.rentedbike;


import data.api.EcoBikeApi;
import data.api.IEcoBikeApi;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class SearchRentedBikeBlackTest {
    IEcoBikeApi api;

    @Before
    public void setUp() {
        api = EcoBikeApi.getInstance();
    }

    @Test
    public void searchParkingTest() {
        Map params = new HashMap();
        params.put("id", "1");
        params.put("name", "user123");
        params.put("address", "Hà Nội");
        assertNotNull(api.searchParking(params));
    }
    @Test
    public void searchParkingWhenParamNullTest() {
        assertNotNull(api.searchParking(null));
    }

    @Test(timeout = 1000)
    public void searchParkingTestTimeout() {
        Map params = new HashMap();
        params.put("id", "1");
        params.put("name", "user123");
        params.put("address", "Hà Nội");
        assertNotNull(api.searchParking(params));
    }
}