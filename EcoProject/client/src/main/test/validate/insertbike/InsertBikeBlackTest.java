package validate.insertbike;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ui.screens.detail.admin.DetailAdminController;
import ui.screens.home.admin.HomeAdminController;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class InsertBikeBlackTest {
    DetailAdminController controller = new DetailAdminController();
    String type;
    String weight;
    String batteryPercentage;
    String loadCycles;
    String estimateUsageHourRemaining;
    String expected;

    public InsertBikeBlackTest(String type, String weight, String batteryPercentage, String loadCycles, String estimateUsageHourRemaining , String expected) {
        super();
        this.type = type;
        this.weight = weight;
        this.batteryPercentage = batteryPercentage;
        this.loadCycles = loadCycles;
        this.estimateUsageHourRemaining = estimateUsageHourRemaining;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> primeNumbers() {
        return Arrays.asList(new Object[][]{
                {"Electric","123a", "10", "2", "2", "Khối lượng không hợp lệ!"},
                {"Electric","123", "10a", "2", "2", "Dung lượng pin không hợp lệ!"},
                {"Electric","123", "10", "2a", "2", "Số lần sạc không hợp lệ!"},
                {"Electric","123", "10", "2", "2a", "Thời gian sử dùng còn lại không hợp lệ!"},
                {"Normal","123", null, null, null, null},
                {"Twins","123a", null, null, null, "Khối lượng không hợp lệ!"},
                {"Normal","123a", null, null, null, "Khối lượng không hợp lệ!"},
                {"Twins","123", null, null, null, null},
        });
    }

    @Test
    public void insertBikeTest() {
        String actual = controller.validateFormBike(type, weight, batteryPercentage, loadCycles, expected);
        assertEquals("tinh gia sai", expected, actual);
    }
}