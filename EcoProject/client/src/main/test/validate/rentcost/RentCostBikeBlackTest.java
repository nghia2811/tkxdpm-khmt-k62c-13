package validate.rentcost;

import data.entity.Bike;
import data.entity.NormalBike;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class RentCostBikeBlackTest {
    Bike bike = new NormalBike();
    Long time;
    Long expected;

    public RentCostBikeBlackTest(Long time, Long expected){
        super();
        this.time = time;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> primeNumbers() {
        return Arrays.asList(new Object[][] {
                { -1L, 0L},
                { 0L, 0L },
                { 1L, 0L },
                { 30L, 10000L },
                { 31L, 10000L },
                { 45L, 13000L },
                { 46L, 13000L },
        });
    }

    @Test
    public void calculateBaseRentCostTest() {
        Long actual =  bike.calculateBaseRentCost(time);
       assertEquals("tinh gia sai",expected, actual);
    }
}