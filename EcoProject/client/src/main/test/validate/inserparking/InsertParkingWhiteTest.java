package validate.inserparking;

import data.entity.Bike;
import data.entity.NormalBike;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ui.screens.home.admin.HomeAdminController;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class InsertParkingWhiteTest {
    HomeAdminController controller = new HomeAdminController();
    String name;
    String address;
    String numberEmptyDocks;
    String expected;

    public InsertParkingWhiteTest(String name, String address, String numberEmptyDocks, String expected) {
        super();
        this.name = name;
        this.address = address;
        this.numberEmptyDocks = numberEmptyDocks;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> primeNumbers() {
        return Arrays.asList(new Object[][]{
                {"", "Hai Bà Trưng, Hà Nội", "10", "Không được bỏ trống tên bãi xe!"},
                {"Bách Khoa", "", "10", "Không được bỏ trống địa chỉ bãi xe!"},
                {"Bách Khoa", "Hai Bà Trưng, Hà Nội", "10a", "Số dock trống không hợp lệ!"},
                {"Bách Khoa", "Hai Bà Trưng, Hà Nội", "10", null},
        });
    }

    @Test
    public void insertParkingTest() {
        String actual = controller.validateFormParking(name, address, numberEmptyDocks);
        assertEquals("tinh gia sai", expected, actual);
    }
}