package result;

import data.entity.Request;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ui.screens.detail.user.DetailUserController;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class OnResultWhiteTest {
    DetailUserController controller = new DetailUserController();
    Request.Result result;
    String expected;

    public OnResultWhiteTest( Request.Result result, String expected){
        super();
        this.result = result;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> primeNumbers() {
        return Arrays.asList(new Object[][] {
                { Request.Result.SUCCESS, "Thuê xe thành công"},
                { Request.Result.NOT_ENOUGH_MONEY, "Không đủ số dư!"},
                { Request.Result.CARD_NOT_EXIST, null},
                { Request.Result.ERROR, "Có lỗi xảy ra"},
        });
    }

    @Test
    public void onResultTest() {
        String actual = controller.onResult(result);
       assertEquals("trả về kết quả sai",expected, actual);
    }

}