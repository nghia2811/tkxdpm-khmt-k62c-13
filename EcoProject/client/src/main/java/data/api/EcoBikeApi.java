package data.api;

import data.entity.*;

import javax.ws.rs.client.*;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

public class EcoBikeApi implements IEcoBikeApi {

    public static final String BASE_URL = "http://localhost:8080";
    private volatile static EcoBikeApi instance;
    private final Client client = ClientBuilder.newClient();

    private EcoBikeApi() {
    }
    public static EcoBikeApi getInstance() {
        if (instance == null)
            synchronized (EcoBikeApi.class) {
                if (instance == null)
                    instance = new EcoBikeApi();
            }
        return instance;
    }

    @Override
    public List<Parking> searchParking(Map<String, String> queryParams) {
        WebTarget webTarget = client.target(BASE_URL)
                .path("parkings");
        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }
        Invocation.Builder builder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = builder.get();

        return response.readEntity(new GenericType<List<Parking>>() {
        });
    }

    @Override
    public long insertParking(Parking parking) {
        WebTarget webTarget = client.target(BASE_URL)
                .path("parkings");
        Invocation.Builder builder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = builder.post(Entity.entity(parking, MediaType.APPLICATION_JSON));
        return response.readEntity(Long.class);
    }

    @Override
    public long updateParking(long id, Parking parking) {
        WebTarget webTarget = client.target(BASE_URL)
                .path("parkings")
                .path(String.valueOf(id));
        Invocation.Builder builder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = builder.put(Entity.entity(parking, MediaType.APPLICATION_JSON));
        return response.readEntity(Long.class);
    }

    @Override
    public long deleteParking(long id) {
        WebTarget webTarget = client.target(BASE_URL)
                .path("parkings")
                .path(String.valueOf(id));

        Invocation.Builder builder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = builder.delete();

        return response.readEntity(Long.class);
    }

    @Override
    public List<Bike> searchBike(long idParking, Map<String, String> queryParams) {
        WebTarget webTarget = client.target(BASE_URL)
                .path("parkings")
                .path(String.valueOf(idParking))
                .path("bikes");

        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }

        Invocation.Builder builder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = builder.get();

        return response.readEntity(new GenericType<List<Bike>>() {
        });
    }

    @Override
    public long insertBike(long idParking, Bike bike) {
        WebTarget webTarget = client.target(BASE_URL)
                .path("parkings")
                .path(String.valueOf(idParking))
                .path("bikes");

        Invocation.Builder builder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = builder.post(Entity.entity(bike, MediaType.APPLICATION_JSON));

        return response.readEntity(Long.class);
    }

    @Override
    public long updateBike(long idParking, long id, Bike bike) {
        WebTarget webTarget = client.target(BASE_URL)
                .path("parkings")
                .path(String.valueOf(idParking))
                .path("bikes")
                .path(String.valueOf(id));

        Invocation.Builder builder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = builder.put(Entity.entity(bike, MediaType.APPLICATION_JSON));

        return response.readEntity(Long.class);
    }

    @Override
    public long deleteBike(long idParking, long id) {
        WebTarget webTarget = client.target(BASE_URL)
                .path("parkings")
                .path(String.valueOf(idParking))
                .path("bikes")
                .path(String.valueOf(id));

        Invocation.Builder builder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = builder.delete();

        return response.readEntity(Long.class);
    }

    @Override
    public List<CreditCard> searchCard(Map<String, String> queryParams) {
        WebTarget webTarget = client.target(BASE_URL)
                .path("cards");

        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }

        Invocation.Builder builder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = builder.get();

        return response.readEntity(new GenericType<List<CreditCard>>() {
        });
    }

    @Override
    public Long insertCard(CreditCard card) {
        WebTarget webTarget = client.target(BASE_URL)
                .path("cards");

        Invocation.Builder builder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = builder.post(Entity.entity(card, MediaType.APPLICATION_JSON));

        return response.readEntity(Long.class);
    }

    @Override
    public List<RentedBike> searchRentedBike(Map<String, String> queryParams) {
        WebTarget webTarget = client.target(BASE_URL)
                .path("rented");
        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                System.out.println("key" + key);
                System.out.println("value"+ value);
                webTarget = webTarget.queryParam(key, value);
            }
        }

        Invocation.Builder builder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = builder.get();

        return response.readEntity(new GenericType<List<RentedBike>>() {
        });
    }

    @Override
    public Request.Result sendRequest(Request request) {
        WebTarget webTarget = client.target(BASE_URL)
                .path("requests");

        Invocation.Builder builder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = builder.post(Entity.entity(request, MediaType.APPLICATION_JSON));

        return response.readEntity(Request.Result.class);
    }
}
