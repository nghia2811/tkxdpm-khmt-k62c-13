package data.api;

import data.entity.*;

import java.util.List;
import java.util.Map;

public interface IEcoBikeApi {
     List<Parking> searchParking(Map<String, String> queryParams);
     long insertParking(Parking parking);
     long updateParking(long id, Parking parking);
     long deleteParking(long id);
    List<Bike> searchBike(long idParking, Map<String, String> queryParams);
    long insertBike(long idParking, Bike bike);
    long updateBike(long idParking, long id, Bike bike);
    long deleteBike(long idParking, long id);
    List<CreditCard> searchCard(Map<String, String> queryParams);
    Long insertCard(CreditCard card);
    List<RentedBike> searchRentedBike(Map<String, String> queryParams);
    Request.Result sendRequest(Request request);

}
