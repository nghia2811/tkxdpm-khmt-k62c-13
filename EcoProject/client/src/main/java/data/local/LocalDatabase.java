package data.local;

import data.entity.User;

public class LocalDatabase {

    private User user = new User(123, "User123");

    public User getUser() {
        return user;
    }
}
