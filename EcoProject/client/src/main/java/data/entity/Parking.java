package data.entity;

import java.util.ArrayList;
import java.util.List;

public class Parking {
    private long id;
    private String name;
    private String address;
    private int numberOfEmptyDocks;
    private List<Bike> bikes = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getNumberOfEmptyDocks() {
        return numberOfEmptyDocks;
    }

    public void setNumberOfEmptyDocks(int numberOfEmptyDocks) {
        this.numberOfEmptyDocks = numberOfEmptyDocks;
    }

    public List<Bike> getBikes() {
        return bikes;
    }

    public void setBikes(List<Bike> bikes) {
        this.bikes = bikes;
    }
}
