package data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("electric")
@JsonIgnoreProperties(ignoreUnknown = true)
public class EBike extends Bike {
    private int batteryPercentage;
    private int loadCycles;
    private int estimateUsageHourRemaining;

    public int getBatteryPercentage() {
        return batteryPercentage;
    }

    public void setBatteryPercentage(int batteryPercentage) {
        this.batteryPercentage = batteryPercentage;
    }

    public int getLoadCycles() {
        return loadCycles;
    }

    public void setLoadCycles(int loadCycles) {
        this.loadCycles = loadCycles;
    }

    public int getEstimateUsageHourRemaining() {
        return estimateUsageHourRemaining;
    }

    public void setEstimateUsageHourRemaining(int estimateUsageHourRemaining) {
        this.estimateUsageHourRemaining = estimateUsageHourRemaining;
    }

    @Override
    public double getRentCostMulti() {
        return 1.5;
    }

    @Override
    public long getDeposit() {
        return 700000;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() +
                " {" +
                super.toString() +
                ", batteryPercentage=" + batteryPercentage +
                ", loadCycles=" + loadCycles +
                ", estimateUsageHourRemaining=" + estimateUsageHourRemaining +
                "}";
    }
}
