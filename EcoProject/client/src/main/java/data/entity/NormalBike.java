package data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("normal")
@JsonIgnoreProperties(ignoreUnknown = true)
public class NormalBike extends Bike {

    @Override
    public double getRentCostMulti() {
        return 1.0;
    }

    @Override
    public long getDeposit() {
        return 400000;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {" + super.toString() + "}";
    }
}
