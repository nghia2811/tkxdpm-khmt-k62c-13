package data.entity;

public class RentedBike {

    private Bike bike;
    private String username;
    private String namePark;
    private long parkingId;
    private long cardNumber;

    public String getNamePark() {
        return namePark;
    }

    public void setNamePark(String namePark) {
        this.namePark = namePark;
    }

    private long rentedTime;

    public Bike getBike() {
        return bike;
    }

    public void setBike(Bike bike) {
        this.bike = bike;
    }

    public long getParkingId() {
        return parkingId;
    }

    public void setParkingId(long parkingId) {
        this.parkingId = parkingId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(long cardNumber) {
        this.cardNumber = cardNumber;
    }

    public long getRentedTime() {
        return rentedTime;
    }

    public void setRentedTime(long rentedTime) {
        this.rentedTime = rentedTime;
    }
}
