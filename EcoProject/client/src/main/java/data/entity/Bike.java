package data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "subType")
@JsonSubTypes({
        @JsonSubTypes.Type(value = EBike.class, name = "electric"),
        @JsonSubTypes.Type(value = NormalBike.class, name = "normal"),
        @JsonSubTypes.Type(value = TwinsBike.class, name = "twins"),
})
@JsonTypeName("bike")
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class Bike {
    private long id;
    private String name;
    private int weight;
    private String licensePlate;
    private String manufacturingDate;
    private String producer;
    private String cost;
    private Status status;
    private Type type;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getManufacturingDate() {
        return manufacturingDate;
    }

    public void setManufacturingDate(String manufacturingDate) {
        this.manufacturingDate = manufacturingDate;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public abstract long getDeposit();

    public abstract double getRentCostMulti();

    public long calculateBaseRentCost(long minute) {
        if (minute < 10)
            return 0;

        if (minute <= 30)
            return 10000;

        long a = minute - 30;

        double div = a / 15.0;

        long k = Math.round(div);

        return 10000 + k * 3000;
    }

    public long getRentCost(long minute) {
        return Math.round(getRentCostMulti() * calculateBaseRentCost(minute));
    }

    @Override
    public String toString() {
        return "id=" + id +
                ", name='" + name + '\'' +
                ", weight=" + weight +
                ", licensePlate='" + licensePlate + '\'' +
                ", manufacturingDate='" + manufacturingDate + '\'' +
                ", producer='" + producer + '\'' +
                ", cost='" + cost + '\'' +
                ", status=" + status +
                ", type=" + type +
                '}';
    }

    public enum Type {
        NORMAL("Normal"),
        ELECTRIC("Electric"),
        TWINS("Twins");

        private static final Map<String, Type> map = new HashMap<>();
        private static final List<String> labels = new ArrayList<>();

        static {
            for (Type t : values()) {
                map.put(t.label, t);
                labels.add(t.label);
            }
        }

        public String label;

        Type(String label) {
            this.label = label;
        }

        public static Type valueOfLabel(String label) {
            return map.get(label);
        }

        public static List<String> getLabels() {
            return labels;
        }
    }

    public enum Status {
        READY("Ready"), RENTED("Rented");

        private static final List<String> labels = new ArrayList<>();

        static {
            for (Status s : values()) {
                labels.add(s.label);
            }
        }

        public String label;

        Status(String label) {
            this.label = label;
        }

        public static List<String> getLabels() {
            return labels;
        }
    }
}
