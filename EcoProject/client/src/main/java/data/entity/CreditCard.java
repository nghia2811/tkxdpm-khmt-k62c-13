package data.entity;

public class CreditCard {

    private long userId;
    private long cardNumber;
    private String owner;
    private String expirationDate = "01/01/2100";
    private long balance = 1000000L;
    private String currencyUnit = "VND";

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(long cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public String getCurrencyUnit() {
        return currencyUnit;
    }

    public void setCurrencyUnit(String currencyUnit) {
        this.currencyUnit = currencyUnit;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof CreditCard)
            return this.cardNumber == ((CreditCard) o).cardNumber;
        return false;
    }

    @Override
    public int hashCode() {
        return (int) (cardNumber ^ (cardNumber >>> 32));
    }
}
