import javafx.application.Application;
import javafx.stage.Stage;
import ui.screens.home.user.HomeUserView;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Eco Bike Rental");
        primaryStage.setResizable(false);
        primaryStage.centerOnScreen();

        primaryStage.setScene(new HomeUserView().getScene());
        primaryStage.show();
    }
}
