package util;

import data.entity.Bike;

import java.util.Map;

public class ValidateUtil {

    public static boolean isValidForm(Map<String, String> map) {
        boolean valid = true;
        for (Map.Entry<String, String> entry : map.entrySet())
            if (entry.getValue() == null || entry.getValue().trim().equals("")) {
                valid = false;
                break;
            }
        return valid;
    }

    public static boolean isValidStringNumber(String s) {
        try {
            Long.parseLong(s);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
