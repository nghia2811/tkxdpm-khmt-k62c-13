package util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeConverter {

    public static long millisToMinute(long millis) {
        double a = (double) millis / (1000 * 60);
        return Math.round(a);
    }


    public static String millisToDateString(long millis) {
        Date date = new Date(millis);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss - dd/MM/yyyy");
        return simpleDateFormat.format(date);
    }
}
