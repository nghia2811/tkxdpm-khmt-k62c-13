package base;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.stage.Stage;
import util.Const;

import java.io.IOException;
import java.util.Optional;

public abstract class BaseView<C extends BaseController> {
    protected C controller;

    private Scene scene;

    public BaseView() {
        onCreateView();
        onCreateController();
        initComponents();
        setupComponents();
        onAttach();
        onStart();
    }

    public abstract String getLayout();

    public abstract void onCreateController();

    public abstract void initComponents();

    public abstract void setupComponents();

    private void onCreateView() {
        try {
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource(getLayout()));
            scene = new Scene(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void onAttach() {
        controller.setView(this);
    }

    public void onStart() {
        controller.onViewStart();
    }

    public ContextMenu createContextMenu() {
        return new ContextMenu();
    }

    public Scene getScene() {
        return scene;
    }

    public void showInfoDialog(String title, String description) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(title);
        alert.setContentText(description);
        alert.show();
    }

    public boolean showConfirmDialog(String title, String description) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText(description);
        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == ButtonType.OK;
    }

    public void showErrorDialog(String title, String description) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        if (description.equals(Const.ERROR_DIALOG_MISSING_FIELD))
            description = "Vui lòng điền đầy đủ các trường!";
        alert.setHeaderText(description);
        alert.show();
    }

    public void switchScene(BaseView baseView) {
        ((Stage) scene.getWindow()).setScene(baseView.getScene());
    }

    public void openNewStage(BaseView baseView) {
        Stage stage = new Stage();
        stage.centerOnScreen();
        stage.setScene(baseView.getScene());
        stage.show();
    }
}
