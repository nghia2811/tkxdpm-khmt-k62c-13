package base;

import data.api.IEcoBikeApi;
import data.local.LocalDatabase;
import data.api.EcoBikeApi;

public abstract class BaseController<V extends BaseView> {
    protected V view;

    public IEcoBikeApi api;
    protected LocalDatabase localDatabase = new LocalDatabase();

    public void setView(V view) {
        this.view = view;
    }

    public abstract void onViewStart();
}
