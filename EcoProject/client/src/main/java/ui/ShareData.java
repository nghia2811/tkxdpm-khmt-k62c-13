package ui;

import java.util.HashMap;
import java.util.Map;

public class ShareData {
    private volatile static ShareData instance;
    private final Map<String, Object> data = new HashMap<>();

    private ShareData() {
    }

    public static ShareData getInstance() {
        if (instance == null)
            synchronized (ShareData.class) {
                if (instance == null)
                    instance = new ShareData();
            }
        return instance;
    }

    public void putValue(String key, Object value) {
        data.put(key, value);
    }

    public Object getValue(String key) {
        return data.get(key);
    }

}
