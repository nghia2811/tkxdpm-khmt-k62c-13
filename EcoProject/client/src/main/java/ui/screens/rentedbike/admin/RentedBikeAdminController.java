package ui.screens.rentedbike.admin;

import ui.screens.rentedbike.RentedBikeController;

public class RentedBikeAdminController extends RentedBikeController<RentedBikeAdminView> {

    @Override
    public void onRefresh() {
        view.showRentedBikeList(api.searchRentedBike(null));
    }
}
