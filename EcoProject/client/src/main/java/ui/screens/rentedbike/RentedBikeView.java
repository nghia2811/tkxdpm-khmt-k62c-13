package ui.screens.rentedbike;

import base.BaseView;
import data.entity.RentedBike;
import javafx.beans.value.ObservableValueBase;
import javafx.collections.FXCollections;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import ui.dialog.FormDialog;
import util.TimeConverter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class RentedBikeView<C extends RentedBikeController> extends BaseView<C> {

    private Button refreshButton;

    private TextField parkingIdTextField;
    private TextField usernameTextField;
    private TextField cardNumberTextField;

    protected TableView<RentedBike> rentedTable;
    private TableColumn<RentedBike, Long> parkingIdColumn;
    private TableColumn<RentedBike, Long> bikeIdColumn;
    private TableColumn<RentedBike, String> userNameColumn;
    private TableColumn<RentedBike, String> rentedTimeColumn;
    private TableColumn<RentedBike, Long> cardNumberColumn;

    @Override
    public String getLayout() {
        return "rented_bike_view.fxml";
    }

    @Override
    public void initComponents() {
        refreshButton = (Button) getScene().lookup("#refresh_button");
        parkingIdTextField = (TextField) getScene().lookup("#parking_id_field");
        usernameTextField = (TextField) getScene().lookup("#user_name_field");
        cardNumberTextField = (TextField) getScene().lookup("#card_number_field");

        rentedTable = (TableView<RentedBike>) getScene().lookup("#rented_table");
        parkingIdColumn = (TableColumn<RentedBike, Long>) rentedTable.getColumns().get(0);
        bikeIdColumn = (TableColumn<RentedBike, Long>) rentedTable.getColumns().get(1);
        userNameColumn = (TableColumn<RentedBike, String>) rentedTable.getColumns().get(2);
        rentedTimeColumn = (TableColumn<RentedBike, String>) rentedTable.getColumns().get(3);
        cardNumberColumn = (TableColumn<RentedBike, Long>) rentedTable.getColumns().get(4);

    }

    @Override
    public void setupComponents() {
        refreshButton.setOnMouseClicked(event -> onClickRefreshButton());
        rentedTable.setRowFactory(tableView -> {
            TableRow<RentedBike> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && !row.isEmpty()) {
                    showDetailRentedBike();
                }
            });
            return row;
        });

        parkingIdColumn.setCellValueFactory(new PropertyValueFactory<>("parkingId"));
        userNameColumn.setCellValueFactory(new PropertyValueFactory<>("username"));
        cardNumberColumn.setCellValueFactory(new PropertyValueFactory<>("cardNumber"));
        bikeIdColumn.setCellValueFactory(param -> new ObservableValueBase<Long>() {
            @Override
            public Long getValue() {
                return param.getValue().getBike().getId();
            }
        });
        rentedTimeColumn.setCellValueFactory(param -> new ObservableValueBase<String>() {
            @Override
            public String getValue() {
                return TimeConverter.millisToDateString(param.getValue().getRentedTime());
            }
        });
        parkingIdTextField.textProperty().addListener((observable, oldValue, newValue) -> onSearchRentedBike());
        usernameTextField.textProperty().addListener((observable, oldValue, newValue) -> onSearchRentedBike());
        cardNumberTextField.textProperty().addListener((observable, oldValue, newValue) -> onSearchRentedBike());
    }

    public void showRentedBikeList(List<RentedBike> rentedBikeList) {
        List<RentedBike> result = rentedBikeList.stream()
                .filter(line -> "user123".equals(line.getUsername()))
                .collect(Collectors.toList());
        rentedTable.setItems(FXCollections.observableList(result));
    }

    private void showDetailRentedBike() {
        FormDialog dialog = new FormDialog();
        dialog.setTitle("Chi tiết");
        RentedBike rentedBike = getSelectedRentedBike();
        if (rentedBike != null) {
            dialog.addField(Arrays.asList("Tên xe", "Kiểu xe", "Khách hàng", "Bãi thuê", "Thời gian sử dụng (phút)"),
                    Arrays.asList(
                            rentedBike.getBike().getName(),
                            rentedBike.getBike().getType().label,
                            rentedBike.getUsername(),
                            rentedBike.getNamePark(),
                            String.valueOf(TimeConverter.millisToMinute(System.currentTimeMillis() - rentedBike.getRentedTime()))));
            dialog.setEditable(false);
            dialog.show();
        }
    }

    private void onClickRefreshButton() {
        controller.onRefresh();
    }

    private void onSearchRentedBike() {
        controller.onSearchRentedBike(createQueryParams());
    }

    private Map<String, String> createQueryParams() {
        Map<String, String> res = new HashMap<>();
        res.put("parking_id", parkingIdTextField.getText().trim());
        res.put("username", usernameTextField.getText().trim());
        res.put("card_number", cardNumberTextField.getText().trim());
        return res;
    }

    private RentedBike getSelectedRentedBike() {
        return rentedTable.getSelectionModel().getSelectedItem();
    }
}
