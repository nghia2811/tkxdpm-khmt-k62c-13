package ui.screens.rentedbike;

import base.BaseController;

import java.util.Map;

public abstract class RentedBikeController<V extends RentedBikeView> extends BaseController<V> {

    @Override
    public void onViewStart() {
        onRefresh();
    }

    public void onSearchRentedBike(Map<String, String> queryParams) {
        view.showRentedBikeList(api.searchRentedBike(queryParams));
    }

    public abstract void onRefresh();
}
