package ui.screens.rentedbike.user;

import data.entity.User;
import ui.screens.rentedbike.RentedBikeController;

import java.util.HashMap;
import java.util.Map;

public class RentedBikeUserController extends RentedBikeController<RentedBikeUserView> {

    @Override
    public void onRefresh() {
        User user = localDatabase.getUser();
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("user_id", String.valueOf(user.getId()));
        view.showRentedBikeList(api.searchRentedBike(queryParams));
    }
}
