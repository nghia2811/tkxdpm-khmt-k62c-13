package ui.screens.rentedbike.user;

import data.api.EcoBikeApi;
import ui.screens.rentedbike.RentedBikeView;

public class RentedBikeUserView extends RentedBikeView<RentedBikeUserController> {

    @Override
    public void onCreateController() {
        controller = new RentedBikeUserController();
        controller.api = EcoBikeApi.getInstance();
    }
}
