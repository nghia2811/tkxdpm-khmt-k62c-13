package ui.screens.rentedbike.admin;

import data.api.EcoBikeApi;
import data.entity.RentedBike;
import javafx.collections.FXCollections;
import ui.screens.rentedbike.RentedBikeView;

import java.util.List;
import java.util.stream.Collectors;

public class RentedBikeAdminView extends RentedBikeView<RentedBikeAdminController> {

    @Override
    public void onCreateController() {
        controller = new RentedBikeAdminController();
        controller.api = EcoBikeApi.getInstance();
    }

    @Override
    public void showRentedBikeList(List<RentedBike> rentedBikeList) {
        rentedTable.setItems(FXCollections.observableList(rentedBikeList));
    }
}
