package ui.screens.paymentinfo;

import base.BaseView;
import data.api.EcoBikeApi;
import data.entity.CreditCard;
import javafx.collections.FXCollections;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.List;

public class PaymentInfoView extends BaseView<PaymentInfoController> {

    private TextField numberTextField;
    private TextField ownerTextField;
    private TextField balanceTextField;

    private Button addButton;

    private TableView<CreditCard> paymentTable;
    private TableColumn<CreditCard, String> ownerColumn;
    private TableColumn<CreditCard, Long> cardNumberColumn;
    private TableColumn<CreditCard, String> expiDateColumn;
    private TableColumn<CreditCard, Long> balanceColumn;
    private TableColumn<CreditCard, String> unitColumn;

    @Override
    public String getLayout() {
        return "payment_info_view.fxml";
    }

    @Override
    public void onCreateController() {
        controller = new PaymentInfoController();
        controller.api = EcoBikeApi.getInstance();
    }

    @Override
    public void initComponents() {
        numberTextField = (TextField) getScene().lookup("#number_text_field");
        ownerTextField = (TextField) getScene().lookup("#owner_text_field");
        balanceTextField = (TextField) getScene().lookup("#balance_text_field");

        addButton = (Button) getScene().lookup("#add_button");

        paymentTable = (TableView<CreditCard>) getScene().lookup("#payment_table");
        ownerColumn = (TableColumn<CreditCard, String>) paymentTable.getColumns().get(0);
        cardNumberColumn = (TableColumn<CreditCard, Long>) paymentTable.getColumns().get(1);
        expiDateColumn = (TableColumn<CreditCard, String>) paymentTable.getColumns().get(2);
        balanceColumn = (TableColumn<CreditCard, Long>) paymentTable.getColumns().get(3);
        unitColumn = (TableColumn<CreditCard, String>) paymentTable.getColumns().get(4);
    }

    @Override
    public void setupComponents() {
        ownerColumn.setCellValueFactory(new PropertyValueFactory<>("owner"));
        cardNumberColumn.setCellValueFactory(new PropertyValueFactory<>("cardNumber"));
        expiDateColumn.setCellValueFactory(new PropertyValueFactory<>("expirationDate"));
        balanceColumn.setCellValueFactory(new PropertyValueFactory<>("balance"));
        unitColumn.setCellValueFactory(new PropertyValueFactory<>("currencyUnit"));

        addButton.setOnMouseClicked(event -> onClickAddCreditCardButton());
    }

    public void showCreditCardList(List<CreditCard> cardList) {
        paymentTable.setItems(FXCollections.observableList(cardList));
    }

    public void onClickAddCreditCardButton() {
        controller.onAddCreditCard(
                numberTextField.getText(),
                ownerTextField.getText(),
                balanceTextField.getText());
    }
}
