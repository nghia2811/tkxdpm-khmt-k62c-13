package ui.screens.paymentinfo;

import base.BaseController;
import data.entity.CreditCard;
import data.entity.User;
import util.ValidateUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PaymentInfoController extends BaseController<PaymentInfoView> {

    @Override
    public void onViewStart() {
        onRefresh();
    }

    public void onRefresh() {
        User user = localDatabase.getUser();

        Map<String, String> query = new HashMap<>();
        query.put("user_id", String.valueOf(user.getId()));

        List<CreditCard> cards = api.searchCard(query);
        view.showCreditCardList(cards);
    }

    public void onAddCreditCard(String cardNumber, String owner, String balance) {
        String error = validateFormCreditCard(cardNumber, balance);
        if(error != null) {
            view.showErrorDialog("", error);
            return;
        }

        User user = localDatabase.getUser();

        CreditCard creditCard = new CreditCard();
        creditCard.setUserId(user.getId());
        creditCard.setCardNumber(Long.parseLong(cardNumber));
        creditCard.setOwner(owner);
        creditCard.setBalance(Long.parseLong(balance));

        if (api.insertCard(creditCard) != -1)
            onRefresh();
        else
            view.showErrorDialog("", "Không thể thêm thẻ tín dụng");
    }

    private String validateFormCreditCard(String cardNumber, String balance) {
        if (!ValidateUtil.isValidStringNumber(cardNumber))
            return "Mã thẻ không hợp lệ!";

        if (!ValidateUtil.isValidStringNumber(balance))
            return "Số dư không hợp lệ!";
        return null;
    }
}
