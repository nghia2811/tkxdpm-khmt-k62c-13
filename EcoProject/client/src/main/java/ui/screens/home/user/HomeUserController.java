package ui.screens.home.user;

import data.entity.RentedBike;
import data.entity.Request;
import data.entity.User;
import ui.screens.home.HomeController;
import util.TimeConverter;
import util.ValidateUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeUserController extends HomeController<HomeUserView> {

    public void onReturnBike(long parkingId, String cardNumber) {
        String error = validateFormCreditCard(cardNumber);
        if(error != null) {
            view.showErrorDialog("", error);
            return;
        }

        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("card_number", cardNumber);

        List<RentedBike> temp = api.searchRentedBike(queryParams);

        if (temp.size() == 0) {
            view.showErrorDialog("", "Thẻ chưa được dùng để thuê xe!");
            return;
        }

        RentedBike rentedBike = temp.get(0);

        long currentTime = System.currentTimeMillis();
        long minute = (TimeConverter.millisToMinute(currentTime - rentedBike.getRentedTime()));
//        long rentCost = rentedBike.getBike().getRentCost(minute);
        long rentCost = rentedBike.getBike().getRentCost(70);

        User user = localDatabase.getUser();

        Request request = new Request();
        request.setId(currentTime);
        request.setUserId(user.getId());
        request.setCardNumber(Long.parseLong(cardNumber));
        request.setParkingId(parkingId);
        request.setBikeId(rentedBike.getBike().getId());
        request.setType(Request.Type.RETURN);
        request.putData("cost", String.valueOf(rentCost));
        request.putData("deposit", String.valueOf(rentedBike.getBike().getDeposit()));
        request.putData("previous_parking", String.valueOf(rentedBike.getParkingId()));

        Request.Result result = api.sendRequest(request);

        switch (result) {
            case SUCCESS:
                view.showInfoDialog("Trả xe thành công!",
                        "\nThời gian thuê: " + TimeConverter.millisToDateString(rentedBike.getRentedTime()) +
                                "\nThời gian trả xe: " + TimeConverter.millisToDateString(currentTime) +
                                "\nThời gian thuê: " + minute + " phút" +
                                "\nTiền thuê xe: " + rentCost + " VND");
                onRefresh();
                break;
            case NOT_ENOUGH_MONEY:
                view.showErrorDialog("", "Không đủ số dư!" +
                        "\nTiền thuê xe: " + rentCost +
                        "\nThời gian thuê: " + minute + " phút");
                break;
            default:
                view.showErrorDialog("", "Có lỗi xảy ra");
        }
    }

    private String validateFormCreditCard(String cardNumber) {
        if (!ValidateUtil.isValidStringNumber(cardNumber))
            return "Mã thẻ không hợp lệ!";
        return null;
    }
}
