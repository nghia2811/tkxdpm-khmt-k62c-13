package ui.screens.home.admin;

import data.entity.Parking;
import ui.screens.home.HomeController;
import util.ValidateUtil;

public class HomeAdminController extends HomeController<HomeAdminView> {

    public void onAddParking(String name, String address, String numberOfEmptyDocks) {
        String error = validateFormParking(name, address, numberOfEmptyDocks);
        if (error != null) {
            view.showErrorDialog("", error);
            return;
        }

        Parking parking = new Parking();
        parking.setName(name);
        parking.setAddress(address);
        parking.setNumberOfEmptyDocks(Integer.parseInt(numberOfEmptyDocks));

        if (api.insertParking(parking) != -1)
            onRefresh();
        else
            view.showErrorDialog("", "Không thể thêm bãi xe!");
    }

    public void onUpdateParking(Parking p, String name, String address) {
        p.setName(name);
        p.setAddress(address);

        if (api.updateParking(p.getId(), p) != -1)
            onRefresh();
        else
            view.showErrorDialog("", "Không thể cập nhật bãi xe!");
    }

    public void onDeleteParking(long id) {
        if (api.deleteParking(id) != -1)
            onRefresh();
        else
            view.showErrorDialog("", "Không thể xóa bãi xe");
    }

    public String validateFormParking(String name, String address, String numberOfEmptyDocks) {
        if (name.isEmpty()) return "Không được bỏ trống tên bãi xe!";
        if (address.isEmpty()) return "Không được bỏ trống địa chỉ bãi xe!";
        if (!ValidateUtil.isValidStringNumber(numberOfEmptyDocks)) {
            return "Số dock trống không hợp lệ!";
        }
        return null;
    }
}
