package ui.screens.home;

import base.BaseView;
import data.entity.Bike;
import data.entity.Parking;
import javafx.beans.value.ObservableValueBase;
import javafx.collections.FXCollections;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import ui.ShareData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class HomeView<C extends HomeController> extends BaseView<C> {

    private Button refreshButton;
    private Button switchButton;
    private Button showRentedBikeButton;

    private TextField idTextField;
    private TextField nameTextField;
    private TextField addressTextField;

    private TableView<Parking> parkingTable;
    private TableColumn<Parking, Integer> idColumn;
    private TableColumn<Parking, String> nameColumn;
    private TableColumn<Parking, String> addressColumn;
    private TableColumn<Parking, Integer> numberOfBikesColumn;
    private TableColumn<Parking, Integer> numberOfEmptyDocksColumn;

    @Override
    public String getLayout() {
        return "home_view.fxml";
    }

    @Override
    public void initComponents() {
        refreshButton = (Button) getScene().lookup("#refresh_button");
        switchButton = (Button) getScene().lookup("#switch_button");
        showRentedBikeButton = (Button) getScene().lookup("#rented_bike_button");

        idTextField = (TextField) getScene().lookup("#id_text_field");
        nameTextField = (TextField) getScene().lookup("#name_text_field");
        addressTextField = (TextField) getScene().lookup("#address_text_field");

        parkingTable = (TableView) getScene().lookup("#parking_table");

        idColumn = (TableColumn<Parking, Integer>) parkingTable.getColumns().get(0);
        nameColumn = (TableColumn<Parking, String>) parkingTable.getColumns().get(1);
        addressColumn = (TableColumn<Parking, String>) parkingTable.getColumns().get(2);
        numberOfBikesColumn = (TableColumn<Parking, Integer>) parkingTable.getColumns().get(3);
        numberOfEmptyDocksColumn = (TableColumn<Parking, Integer>) parkingTable.getColumns().get(4);
    }

    @Override
    public void setupComponents() {
        refreshButton.setOnMouseClicked(event -> onClickRefreshButton());
        switchButton.setOnMouseClicked(event -> onClickSwitchButton());
        showRentedBikeButton.setOnMouseClicked(event -> goToRentedBikeScreen());

        idTextField.setTextFormatter(new TextFormatter<>(change -> {
            String text = change.getText();
            if (text.matches("[0-9]*"))
                return change;
            return null;
        }));
        idTextField.textProperty().addListener((observable, oldValue, newValue) -> onClickSearchParkingButton());
        nameTextField.textProperty().addListener((observable, oldValue, newValue) -> onClickSearchParkingButton());
        addressTextField.textProperty().addListener((observable, oldValue, newValue) -> onClickSearchParkingButton());

        parkingTable.setRowFactory(tableView -> {
            TableRow<Parking> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && !row.isEmpty()) {
                    goToDetailScreen();
                }
            });
            return row;
        });
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        addressColumn.setCellValueFactory(new PropertyValueFactory<>("address"));
        numberOfBikesColumn.setCellValueFactory(new PropertyValueFactory<>("numberOfBikes"));
        numberOfEmptyDocksColumn.setCellValueFactory(new PropertyValueFactory<>("numberOfEmptyDocks"));
        numberOfBikesColumn.setCellValueFactory(param -> new ObservableValueBase<Integer>() {
            @Override
            public Integer getValue() {
                int count = 0;
                for (Bike b : param.getValue().getBikes())
                    if (b.getStatus() == Bike.Status.READY)
                        ++count;
                return count;
            }
        });

        ContextMenu contextMenu = createContextMenu();
        parkingTable.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (event.getButton() == MouseButton.SECONDARY)
                contextMenu.show(parkingTable, event.getScreenX(), event.getScreenY());
        });
    }

    public abstract void onClickSwitchButton();

    private void onClickSearchParkingButton() {
        controller.onSearchParking(createQueryParams());
    }

    private void onClickRefreshButton() {
        controller.onRefresh();
    }

    public void showParkingList(List<Parking> parkingList) {
        parkingTable.setItems(FXCollections.observableList(parkingList));
    }

    public Parking getSelectedParking() {
        return parkingTable.getSelectionModel().getSelectedItem();
    }

    public void goToDetailScreen() {
        Parking parking = getSelectedParking();
        ShareData shareData = ShareData.getInstance();
        shareData.putValue("id_parking", parking.getId());
    }

    public abstract void goToRentedBikeScreen();

    private Map<String, String> createQueryParams() {
        Map<String, String> res = new HashMap<>();
        res.put("id", idTextField.getText().trim());
        res.put("name", nameTextField.getText().trim());
        res.put("" +
                "", addressTextField.getText().trim());
        return res;
    }

}
