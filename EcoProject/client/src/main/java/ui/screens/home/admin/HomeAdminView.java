package ui.screens.home.admin;

import data.api.EcoBikeApi;
import data.entity.Parking;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import ui.dialog.FormDialog;
import ui.screens.detail.admin.DetailAdminView;
import ui.screens.home.HomeView;
import ui.screens.home.user.HomeUserView;
import ui.screens.rentedbike.admin.RentedBikeAdminView;
import util.Const;
import util.ValidateUtil;

import java.util.Arrays;

public class HomeAdminView extends HomeView<HomeAdminController> {

    @Override
    public void onCreateController() {
        controller = new HomeAdminController();
        controller.api = EcoBikeApi.getInstance();
    }

    @Override
    public ContextMenu createContextMenu() {
        ContextMenu contextMenu = super.createContextMenu();

        String nameField = "Tên bãi xe";
        String addressField = "Địa chỉ";
        String emptyDockField = "Số dock trống";

        MenuItem item1 = new MenuItem("Thêm bãi xe");
        item1.setOnAction(event -> {
            FormDialog dialog = new FormDialog();
            dialog.setTitle(item1.getText());
            dialog.addField(Arrays.asList(nameField, addressField, emptyDockField), null);

            dialog.showAndWait().ifPresent(map -> {
                if (ValidateUtil.isValidForm(map))
                    onClickAddParkingButton(map.get(nameField), map.get(addressField), map.get(emptyDockField));
                else
                    showErrorDialog("",Const.ERROR_DIALOG_MISSING_FIELD);
            });
        });

        MenuItem item2 = new MenuItem("Sửa thông tin bãi xe");
        item2.setOnAction(event -> {
            Parking p = getSelectedParking();
            if (p == null) {
                showErrorDialog("","Vui lòng chọn bãi xe!");
            } else {
                FormDialog dialog = new FormDialog();
                dialog.setTitle(item2.getText());
                dialog.addField(Arrays.asList(nameField, addressField),
                        Arrays.asList(p.getName(), p.getAddress()));

                dialog.showAndWait().ifPresent(map -> {
                    if (ValidateUtil.isValidForm(map)) {
                        onClickUpdateParkingButton(map.get(nameField), map.get(addressField));
                    } else
                        showErrorDialog("",Const.ERROR_DIALOG_MISSING_FIELD);
                });
            }
        });

        MenuItem item3 = new MenuItem("Xóa bãi xe");
        item3.setOnAction(event -> {
            Parking p = getSelectedParking();
            if (p == null) {
                showErrorDialog("","Vui lòng chọn bãi xe!");
            } else {
                boolean result = showConfirmDialog("","Xóa bãi xe này?");
                if (result)
                    onClickDeleteParkingButton();
            }
        });

        contextMenu.getItems().addAll(item1, item2, item3);

        return contextMenu;
    }

    private void onClickAddParkingButton(String name, String address, String numberOfEmptyDocks) {
        controller.onAddParking(name, address, numberOfEmptyDocks);
    }

    private void onClickUpdateParkingButton(String name, String address) {
        controller.onUpdateParking(getSelectedParking(), name, address);
    }

    private void onClickDeleteParkingButton() {
        controller.onDeleteParking(getSelectedParking().getId());
    }

    @Override
    public void onClickSwitchButton() {
        switchScene(new HomeUserView());
    }

    @Override
    public void goToRentedBikeScreen() {
        openNewStage(new RentedBikeAdminView());
    }

    @Override
    public void goToDetailScreen() {
        super.goToDetailScreen();
        switchScene(new DetailAdminView());
    }
}
