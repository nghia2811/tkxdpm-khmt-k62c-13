package ui.screens.home.user;

import data.api.EcoBikeApi;
import data.entity.Parking;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import ui.dialog.FormDialog;
import ui.screens.detail.user.DetailUserView;
import ui.screens.home.HomeView;
import ui.screens.home.admin.HomeAdminView;
import ui.screens.paymentinfo.PaymentInfoView;
import ui.screens.rentedbike.user.RentedBikeUserView;
import util.Const;
import util.ValidateUtil;

import java.util.Arrays;

public class HomeUserView extends HomeView<HomeUserController> {

    private Button paymentInfoButton;

    @Override
    public void initComponents() {
        super.initComponents();
        paymentInfoButton = (Button) getScene().lookup("#payment_info_button");
    }

    @Override
    public void setupComponents() {
        super.setupComponents();
        paymentInfoButton.setVisible(true);
        paymentInfoButton.setOnMouseClicked(event -> goToPaymentInfoScreen());
    }

    @Override
    public void onCreateController() {
        controller = new HomeUserController();
        controller.api = EcoBikeApi.getInstance();
    }

    @Override
    public ContextMenu createContextMenu() {
        ContextMenu contextMenu = super.createContextMenu();

        String cardNumberField = "Mã thẻ";

        MenuItem item = new MenuItem("Trả xe");
        item.setOnAction(event -> {
            Parking parking = getSelectedParking();

            if (parking == null) {
                showErrorDialog("","Vui lòng chọn bãi xe!");
            } else {
                if (parking.getNumberOfEmptyDocks() == 0)
                    showErrorDialog("","Bãi xe không còn chỗ trống để trả!");
                else {

                    FormDialog dialog = new FormDialog();
                    dialog.setHeaderText("Nhập mã thẻ tín dụng");

                    dialog.addField(Arrays.asList(cardNumberField), null);
                    dialog.showAndWait().ifPresent(map -> {
                        if (ValidateUtil.isValidForm(map))
                            onClickReturnBikeButton(map.get(cardNumberField));
                        else
                            showErrorDialog("",Const.ERROR_DIALOG_MISSING_FIELD);
                    });
                }
            }
        });

        contextMenu.getItems().add(item);

        return contextMenu;
    }


    private void onClickReturnBikeButton(String cardNumber) {
        controller.onReturnBike(getSelectedParking().getId(), cardNumber);
    }

    @Override
    public void onClickSwitchButton() {
        switchScene(new HomeAdminView());
    }

    @Override
    public void goToDetailScreen() {
        super.goToDetailScreen();
        switchScene(new DetailUserView());
    }

    @Override
    public void goToRentedBikeScreen() {
        openNewStage(new RentedBikeUserView());
    }

    public void goToPaymentInfoScreen() {
        openNewStage(new PaymentInfoView());
    }
}
