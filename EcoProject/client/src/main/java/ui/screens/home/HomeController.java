package ui.screens.home;

import base.BaseController;
import data.entity.Parking;

import java.util.List;
import java.util.Map;

public abstract class HomeController<V extends HomeView> extends BaseController<V> {

    @Override
    public void onViewStart() {
        onSearchParking(null);
    }

    public void onSearchParking(Map<String, String> queryParams) {
        List<Parking> result = api.searchParking(queryParams);
        view.showParkingList(result);
    }

    public void onRefresh() {
        view.showParkingList(api.searchParking(null));
    }
}
