package ui.screens.detail;

import base.BaseView;
import data.entity.Bike;
import data.entity.EBike;
import javafx.collections.FXCollections;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import ui.dialog.FormDialog;

import java.util.*;


public abstract class DetailView<C extends DetailController> extends BaseView<C> {
    private Button refreshButton;
    private Button backButton;

    private TextField idTextField;
    private TextField nameTextField;
    private TextField producerTextField;

    private ComboBox<String> typeComboBox;
    private ComboBox<String> statusComboBox;

    private TableView<Bike> bikeTable;
    private TableColumn<Bike, Integer> idColumn;
    private TableColumn<Bike, String> nameColumn;
    private TableColumn<Bike, Integer> weightColumn;
    private TableColumn<Bike, String> licenseColumn;
    private TableColumn<Bike, String> manuDateColumn;
    private TableColumn<Bike, String> producerColumn;
    private TableColumn<Bike, String> costColumn;
    private TableColumn<Bike, Bike.Type> typeColumn;
    private TableColumn<Bike, Bike.Status> statusColumn;

    @Override
    public String getLayout() {
        return "detail_view.fxml";
    }

    @Override
    public void initComponents() {
        backButton = (Button) getScene().lookup("#back_button");
        refreshButton = (Button) getScene().lookup("#refresh_button");
        idTextField = (TextField) getScene().lookup("#id_text_field");
        nameTextField = (TextField) getScene().lookup("#name_text_field");
        producerTextField = (TextField) getScene().lookup("#producer_text_field");
        typeComboBox = (ComboBox<String>) getScene().lookup("#type_combo_box");
        statusComboBox = (ComboBox<String>) getScene().lookup("#status_combo_box");

        bikeTable = (TableView<Bike>) getScene().lookup("#bike_table");
        idColumn = (TableColumn<Bike, Integer>) bikeTable.getColumns().get(0);
        nameColumn = (TableColumn<Bike, String>) bikeTable.getColumns().get(1);
        weightColumn = (TableColumn<Bike, Integer>) bikeTable.getColumns().get(2);
        licenseColumn = (TableColumn<Bike, String>) bikeTable.getColumns().get(3);
        manuDateColumn = (TableColumn<Bike, String>) bikeTable.getColumns().get(4);
        producerColumn = (TableColumn<Bike, String>) bikeTable.getColumns().get(5);
        costColumn = (TableColumn<Bike, String>) bikeTable.getColumns().get(6);
        typeColumn = (TableColumn<Bike, Bike.Type>) bikeTable.getColumns().get(7);
        statusColumn = (TableColumn<Bike, Bike.Status>) bikeTable.getColumns().get(8);
    }

    @Override
    public void setupComponents() {
        idTextField.setTextFormatter(new TextFormatter<>(change -> {
            String text = change.getText();
            if (text.matches("[0-9]*"))
                return change;
            return null;
        }));
        idTextField.textProperty().addListener((observable, oldValue, newValue) -> onClickSearchBikeButton());
        nameTextField.textProperty().addListener((observable, oldValue, newValue) -> onClickSearchBikeButton());
        producerTextField.textProperty().addListener((observable, oldValue, newValue) -> onClickSearchBikeButton());

        List<String> typeLabels = new ArrayList<>(Bike.Type.getLabels());
        typeLabels.add("Any");

        typeComboBox.setItems(FXCollections.observableList(typeLabels));
        typeComboBox.setOnAction(event -> controller.onSearchBike(createQueryParams()));

        List<String> statusLabels = new ArrayList<>(Bike.Status.getLabels());
        statusLabels.add("Any");

        statusComboBox.setItems(FXCollections.observableList(statusLabels));
        statusComboBox.setOnAction(event -> controller.onSearchBike(createQueryParams()));

        bikeTable.setRowFactory(tableView -> {
            TableRow<Bike> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && !row.isEmpty()) {
                    showBikeDetail();
                }
            });
            return row;
        });
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        weightColumn.setCellValueFactory(new PropertyValueFactory<>("weight"));
        licenseColumn.setCellValueFactory(new PropertyValueFactory<>("licensePlate"));
        manuDateColumn.setCellValueFactory(new PropertyValueFactory<>("manufacturingDate"));
        producerColumn.setCellValueFactory(new PropertyValueFactory<>("producer"));
        costColumn.setCellValueFactory(new PropertyValueFactory<>("cost"));
        typeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        statusColumn.setCellValueFactory(new PropertyValueFactory<>("status"));

        ContextMenu contextMenu = createContextMenu();

        bikeTable.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (event.getButton() == MouseButton.SECONDARY)
                contextMenu.show(bikeTable, event.getScreenX(), event.getScreenY());
        });

        backButton.setOnMouseClicked(event -> onClickBackButton());
        refreshButton.setOnMouseClicked(event -> onClickRefreshButton());
    }

    public abstract void onClickBackButton();

    private void onClickSearchBikeButton() {
        controller.onSearchBike(createQueryParams());
    }

    private void onClickRefreshButton() {
        controller.onRefresh();
    }

    public void showBikeList(List<Bike> bikeList) {
        bikeTable.setItems(FXCollections.observableList(bikeList));
    }

    private void showBikeDetail() {
        Bike bike = getSelectedBike();
        FormDialog dialog = new FormDialog();
        dialog.setTitle("Chi tiết");

        if (bike != null) {
            dialog.addField(Arrays.asList("Tiền cọc"), Arrays.asList(String.valueOf(bike.getDeposit())));

            if (bike.getType() == Bike.Type.ELECTRIC) {
                EBike eBike = (EBike) bike;
                dialog.addField(Arrays.asList("Dung lượng pin hiện tại", "Số lần sạc", "Thời gian ước tính còn lại"),
                        Arrays.asList(String.valueOf(eBike.getBatteryPercentage()),
                                String.valueOf(eBike.getLoadCycles()),
                                String.valueOf(eBike.getEstimateUsageHourRemaining())));
            }
            dialog.setEditable(false);
            dialog.show();
        }
    }

    public Bike getSelectedBike() {
        return bikeTable.getSelectionModel().getSelectedItem();
    }

    private Map<String, String> createQueryParams() {
        Map<String, String> res = new HashMap<>();
        res.put("id", idTextField.getText().trim());
        res.put("name", nameTextField.getText().trim());
        res.put("producer", producerTextField.getText().trim());
        res.put("type", typeComboBox.getValue());
        res.put("status", statusComboBox.getValue());
        return res;
    }

}
