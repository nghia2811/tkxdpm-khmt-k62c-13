package ui.screens.detail.user;

import data.api.EcoBikeApi;
import data.entity.Bike;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import ui.dialog.FormDialog;
import ui.screens.detail.DetailView;
import ui.screens.home.user.HomeUserView;
import ui.screens.paymentinfo.PaymentInfoView;
import util.Const;
import util.ValidateUtil;

import java.util.Arrays;

public class DetailUserView extends DetailView<DetailUserController> {

    @Override
    public void onCreateController() {
        controller = new DetailUserController();
        controller.api = EcoBikeApi.getInstance();
    }

    @Override
    public ContextMenu createContextMenu() {
        ContextMenu contextMenu = super.createContextMenu();

        String cardNumberField = "Mã thẻ";

        MenuItem item = new MenuItem("Thuê xe");
        item.setOnAction(event -> {
            Bike bike = getSelectedBike();

            if (bike == null)
                showErrorDialog("","Vui lòng chọn xe!");
            else {
                if (bike.getStatus() == Bike.Status.READY) {
                    FormDialog dialog = new FormDialog();
                    dialog.setHeaderText("Nhập mã thẻ tín dụng");
                    dialog.addField(Arrays.asList(cardNumberField), null);
                    dialog.showAndWait().ifPresent(map -> {
                        if (ValidateUtil.isValidForm(map))
                            onClickRentBikeButton(map.get(cardNumberField));
                        else
                            showErrorDialog("",Const.ERROR_DIALOG_MISSING_FIELD);
                    });
                } else
                    showErrorDialog("","Xe đã được cho thuê!");
            }
        });

        contextMenu.getItems().add(item);

        return contextMenu;
    }

    private void onClickRentBikeButton(String cardNumber) {
        controller.onRentBike(getSelectedBike(), cardNumber);
    }

    @Override
    public void onClickBackButton() {
        switchScene(new HomeUserView());
    }

    public void suggestAddCreditCard() {
        boolean result = showConfirmDialog("", "Thẻ chưa được thêm vào tài khoản. Bạn có muốn thêm thẻ không?");
        if(result)
            goToPaymentInfoScreen();
    }

    public void goToPaymentInfoScreen() {
        openNewStage(new PaymentInfoView());
    }
}
