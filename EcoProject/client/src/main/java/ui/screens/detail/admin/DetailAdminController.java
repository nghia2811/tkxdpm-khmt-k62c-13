package ui.screens.detail.admin;

import data.entity.Bike;
import data.entity.EBike;
import data.entity.NormalBike;
import data.entity.TwinsBike;
import ui.screens.detail.DetailController;
import util.ValidateUtil;

public class DetailAdminController extends DetailController<DetailAdminView> {

    public void onAddBike(String name, String weight, String license, String manufacturingDate,
                          String producer, String cost, String type, String batteryPercentage,
                          String loadCycles, String estimateUsageHourRemaining) {
        String error = validateFormBike(type, weight, batteryPercentage, loadCycles, estimateUsageHourRemaining);
        if (error != null) {
            view.showErrorDialog("", error);
            return;
        }

        Bike bike;
        Bike.Type realType = Bike.Type.valueOfLabel(type);

        switch (realType) {
            case ELECTRIC:
                bike = new EBike();
                ((EBike) bike).setBatteryPercentage(Integer.parseInt(batteryPercentage));
                ((EBike) bike).setLoadCycles(Integer.parseInt(loadCycles));
                ((EBike) bike).setEstimateUsageHourRemaining(Integer.parseInt(estimateUsageHourRemaining));
                break;
            case TWINS:
                bike = new TwinsBike();
                break;
            default:
                bike = new NormalBike();
                break;
        }
        bike.setName(name);
        bike.setWeight(Integer.parseInt(weight));
        bike.setLicensePlate(license);
        bike.setManufacturingDate(manufacturingDate);
        bike.setProducer(producer);
        bike.setCost(cost);
        bike.setType(realType);
        bike.setStatus(Bike.Status.READY);
        if (api.insertBike(idParking, bike) != -1)
            onRefresh();
        else
            view.showErrorDialog("", "Không thể thêm xe!");
    }

    public void onUpdateBike(Bike bike, String name, String weight, String license, String manufacturingDate,
                             String producer, String cost, String batteryPercentage, String loadCycles,
                             String estimateUsageHourRemaining) {

        String error = validateFormBike(bike.getType().label, weight, batteryPercentage, loadCycles, estimateUsageHourRemaining);

        if (error != null) {
            view.showErrorDialog("", error);
            return;
        }

        if (bike.getType() == Bike.Type.ELECTRIC) {
            ((EBike) bike).setBatteryPercentage(Integer.parseInt(batteryPercentage));
            ((EBike) bike).setLoadCycles(Integer.parseInt(loadCycles));
            ((EBike) bike).setEstimateUsageHourRemaining(Integer.parseInt(estimateUsageHourRemaining));
        }

        bike.setName(name);
        bike.setWeight(Integer.parseInt(weight));
        bike.setLicensePlate(license);
        bike.setManufacturingDate(manufacturingDate);
        bike.setProducer(producer);
        bike.setCost(cost);

        if (api.updateBike(idParking, bike.getId(), bike) != -1)
            onRefresh();
        else
            view.showErrorDialog("", "Không thể cập nhật xe!");
    }

    public void onDeleteBike(long id) {
        if (api.deleteBike(idParking, id) != -1)
            onRefresh();
        else
            view.showErrorDialog("", "Không thể xóa xe!");
    }

    public String validateFormBike(
            String type,
            String weight,
            String batteryPercentage,
            String loadCycles,
            String estimateUsageHourRemaining) {

        if (!ValidateUtil.isValidStringNumber(weight)) {
            return "Khối lượng không hợp lệ!";
        }

        Bike.Type realType = Bike.Type.valueOfLabel(type);

        if (realType.equals(Bike.Type.ELECTRIC)) {
            if (!ValidateUtil.isValidStringNumber(batteryPercentage)) {
                return "Dung lượng pin không hợp lệ!";
            }
            if (!ValidateUtil.isValidStringNumber(loadCycles)) {
                return "Số lần sạc không hợp lệ!";
            }
            if (!ValidateUtil.isValidStringNumber(estimateUsageHourRemaining)) {
                return "Thời gian sử dùng còn lại không hợp lệ!";
            }
        }
        return null;
    }
}
