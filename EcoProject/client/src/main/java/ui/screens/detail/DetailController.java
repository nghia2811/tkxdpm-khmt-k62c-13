package ui.screens.detail;

import base.BaseController;
import data.entity.Bike;
import ui.ShareData;

import java.util.List;
import java.util.Map;

public abstract class DetailController<V extends DetailView> extends BaseController<V> {

    protected long idParking;

    @Override
    public void onViewStart() {
        ShareData shareData = ShareData.getInstance();
        idParking = (Long) shareData.getValue("id_parking");

        List<Bike> bikes = api.searchBike(idParking, null);
        view.showBikeList(bikes);
    }

    public void onSearchBike(Map<String, String> queryParams) {
        List<Bike> result = api.searchBike(idParking, queryParams);
        view.showBikeList(result);
    }

    public void onRefresh() {
        List<Bike> result = api.searchBike(idParking, null);
        view.showBikeList(result);
    }
}
