package ui.screens.detail.admin;

import data.api.EcoBikeApi;
import data.entity.Bike;
import data.entity.EBike;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import ui.dialog.FormDialog;
import ui.screens.detail.DetailView;
import ui.screens.home.admin.HomeAdminView;
import util.Const;
import util.ValidateUtil;

import java.util.Arrays;

public class DetailAdminView extends DetailView<DetailAdminController> {

    @Override
    public void onCreateController() {
        controller = new DetailAdminController();
        controller.api = EcoBikeApi.getInstance();
    }

    @Override
    public ContextMenu createContextMenu() {
        ContextMenu contextMenu = super.createContextMenu();

        String nameField = "Tên xe";
        String weightField = "Khối lượng";
        String licenseField = "Biển số xe";
        String manuDateField = "Ngày sản xuất";
        String producerField = "Nhà sản xuất";
        String costField = "Giá thành";
        String typeField = "Kiểu xe";
        String batteryPercentageField = "Dung lượng pin hiện tại";
        String loadCyclesField = "Số lần sử dụng";
        String estimateUsageHourRemainingField = "Số giờ sử dụng còn lại";

        MenuItem item1 = new MenuItem("Thêm xe");
        item1.setOnAction(event -> {
            FormDialog dialog1 = new FormDialog();
            dialog1.setTitle(item1.getText());
            dialog1.setHeaderText("Chọn loại xe");
            dialog1.addComboBox(typeField, null, Bike.Type.getLabels());
            dialog1.showAndWait().ifPresent(map -> {
                FormDialog dialog2 = new FormDialog();
                dialog2.setTitle(item1.getText());

                String type = map.get(typeField);

                if (type.equals(Bike.Type.ELECTRIC.label)) {
                    dialog2.addField(Arrays.asList(nameField, weightField, licenseField, manuDateField, producerField, costField, batteryPercentageField, loadCyclesField, estimateUsageHourRemainingField), null);
                } else
                    dialog2.addField(Arrays.asList(nameField, weightField, licenseField, manuDateField, producerField, costField), null);

                dialog2.showAndWait().ifPresent(map2 -> {
                    if (ValidateUtil.isValidForm(map2))
                        onClickAddBikeButton(map2.get(nameField),
                                map2.get(weightField),
                                map2.get(licenseField),
                                map2.get(manuDateField),
                                map2.get(producerField),
                                map2.get(costField),
                                type,
                                map2.get(batteryPercentageField),
                                map2.get(loadCyclesField),
                                map2.get(estimateUsageHourRemainingField));
                    else
                        showErrorDialog("",Const.ERROR_DIALOG_MISSING_FIELD);
                });
            });

        });

        MenuItem item2 = new MenuItem("Sửa thông tin xe");
        item2.setOnAction(event -> {
            Bike bike = getSelectedBike();

            if (bike == null) {
                showErrorDialog("","Vui lòng chọn xe!");
            } else {

                FormDialog dialog = new FormDialog();
                dialog.setTitle(item2.getText());

                if (bike.getType() == Bike.Type.ELECTRIC) {
                    EBike temp = (EBike) bike;
                    dialog.addField(Arrays.asList(nameField, weightField, licenseField, manuDateField, producerField, costField, batteryPercentageField, loadCyclesField, estimateUsageHourRemainingField),
                            Arrays.asList(bike.getName(), String.valueOf(bike.getWeight()), bike.getLicensePlate(), bike.getManufacturingDate(), bike.getProducer(), bike.getCost(), String.valueOf(temp.getBatteryPercentage()), String.valueOf(temp.getLoadCycles()), String.valueOf(temp.getEstimateUsageHourRemaining())));
                } else {
                    dialog.addField(Arrays.asList(nameField, weightField, licenseField, manuDateField, producerField, costField),
                            Arrays.asList(bike.getName(), String.valueOf(bike.getWeight()), bike.getLicensePlate(), bike.getManufacturingDate(), bike.getProducer(), bike.getCost()));
                }

                dialog.showAndWait().ifPresent(map -> {
                    if (ValidateUtil.isValidForm(map)) {
                        onClickUpdateBikeButton(map.get(nameField), map.get(weightField), map.get(licenseField), map.get(manuDateField), map.get(producerField), map.get(costField), map.get(batteryPercentageField), map.get(loadCyclesField), map.get(estimateUsageHourRemainingField));
                    } else
                        showErrorDialog("",Const.ERROR_DIALOG_MISSING_FIELD);
                });
            }
        });

        MenuItem item3 = new MenuItem("Xóa xe");
        item3.setOnAction(event -> {
            Bike bike = getSelectedBike();

            if (bike == null) {
                showErrorDialog("", "Vui lòng chọn xe!");
            } else {
                boolean result = showConfirmDialog("", "Xóa xe này?");
                if (result)
                    onClickDeleteBikeButton();
            }
        });

        contextMenu.getItems().addAll(item1, item2, item3);

        return contextMenu;
    }

    private void onClickAddBikeButton(String name, String weight, String license, String manufacturingDate,
                                      String producer, String cost, String type, String batteryPercentage,
                                      String loadCycles, String estimateUsageHourRemaining) {
        controller.onAddBike(name, weight, license, manufacturingDate, producer, cost, type, batteryPercentage, loadCycles, estimateUsageHourRemaining);
    }

    private void onClickUpdateBikeButton(String name, String weight, String license, String manufacturingDate,
                                         String producer, String cost, String batteryPercentage,
                                         String loadCycles, String estimateUsageHourRemaining) {
        controller.onUpdateBike(getSelectedBike(), name, weight, license, manufacturingDate, producer, cost, batteryPercentage, loadCycles, estimateUsageHourRemaining);
    }

    private void onClickDeleteBikeButton() {
        controller.onDeleteBike(getSelectedBike().getId());
    }

    @Override
    public void onClickBackButton() {
        switchScene(new HomeAdminView());
    }
}
