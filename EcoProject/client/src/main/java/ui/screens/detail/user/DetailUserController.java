package ui.screens.detail.user;

import data.entity.Bike;
import data.entity.RentedBike;
import data.entity.Request;
import data.entity.User;
import ui.screens.detail.DetailController;
import util.ValidateUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DetailUserController extends DetailController<DetailUserView> {

    public void onRentBike(Bike bike, String cardNumber) {
        String error = validateFormCreditCard(cardNumber);
        if (error != null) {
            view.showErrorDialog("", error);
            return;
        }

        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("card_number", cardNumber);

        List<RentedBike> temp = api.searchRentedBike(queryParams);
        if (temp.size() > 0) {
            view.showErrorDialog("", "Thẻ đã được dùng để thuê xe khác!");
            return;
        }

        User user = localDatabase.getUser();

        Request request = new Request();
        request.setId(System.currentTimeMillis());
        request.setCardNumber(Long.parseLong(cardNumber));
        request.setUserId(user.getId());
        request.setBikeId(bike.getId());
        request.setParkingId(idParking);
        request.setType(Request.Type.RENT);
        request.putData("deposit", String.valueOf(bike.getDeposit()));

        Request.Result result = api.sendRequest(request);
        String message = onResult(result);
        if (message == null) view.suggestAddCreditCard();
        else{
            view.showInfoDialog("", message);
        }
        onRefresh();
    }

    public String onResult(Request.Result result) {
        switch (result) {
            case SUCCESS:
                return "Thuê xe thành công";
            case NOT_ENOUGH_MONEY:
                return "Không đủ số dư!";
            case CARD_NOT_EXIST:
                return null;
            default:
                return "Có lỗi xảy ra";
        }
    }

    private String validateFormCreditCard(String cardNumber) {
        if (!ValidateUtil.isValidStringNumber(cardNumber))
            return "Mã thẻ không hợp lệ!";
        return null;
    }
}
