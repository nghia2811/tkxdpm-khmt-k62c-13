package ui.dialog;

import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

public class FormDialog extends Dialog<Map<String, String>> {

    private final GridPane gridPane;

    private final ButtonType doneButtonType;
    private final Button doneButton;

    private final Map<Label, Node> map = new HashMap<>();

    private int numberOfFields = 0;

    public FormDialog() {
        super();

        doneButtonType = new ButtonType("Done", ButtonBar.ButtonData.OK_DONE);
        getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, doneButtonType);

        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(20, 20, 20, 20));

        doneButton = (Button) getDialogPane().lookupButton(doneButtonType);

        setResultConverter(param -> {
            if (param == doneButtonType) {
                Map<String, String> result = new HashMap<>();
                map.forEach((label, node) -> {
                    if (node instanceof TextField)
                        result.put(label.getText(), ((TextField) node).getText());
                    else if (node instanceof ComboBox)
                        result.put(label.getText(), ((ComboBox<String>) node).getValue());
                });

                return result;
            }
            return null;
        });

        getDialogPane().setContent(gridPane);
    }

    public void addLabel(String... labels) {
        for (String label : labels) {
            gridPane.add(new Label(label), 0, numberOfFields);
            ++numberOfFields;
        }
    }

    public void addField(List<String> fields, List<String> initialValues) {
        List<TextField> temp = new ArrayList<>();

        for (String field : fields) {
            Label label = new Label(field);
            gridPane.add(label, 0, numberOfFields);

            TextField textField = new TextField();
            textField.setPromptText(field);
            gridPane.add(textField, 1, numberOfFields);

            map.put(label, textField);
            temp.add(textField);

            ++numberOfFields;
        }

        if (initialValues != null)
            for (int i = 0; i < initialValues.size(); ++i) {
                temp.get(i).setText(initialValues.get(i));
            }
    }

    public void addComboBox(String field, String initialValues, List<String> options) {
        ComboBox<String> comboBox = new ComboBox<>();
        comboBox.setItems(FXCollections.observableList(options));

        if (initialValues == null)
            comboBox.getSelectionModel().selectFirst();
        else {
            comboBox.setPromptText(initialValues);
            for (int i = 0; i < comboBox.getItems().size(); ++i)
                if (comboBox.getItems().get(i).equals(initialValues)) {
                    comboBox.getSelectionModel().select(i);
                    break;
                }
        }

        Label label = new Label(field);

        gridPane.add(label, 0, numberOfFields);
        gridPane.add(comboBox, 1, numberOfFields);

        map.put(label, comboBox);
    }

    public void setEditable(Boolean editable) {
        if (!editable)
            map.forEach((label, node) -> {
                if (node instanceof TextField)
                    ((TextField) node).setEditable(false);
            });
    }
}
