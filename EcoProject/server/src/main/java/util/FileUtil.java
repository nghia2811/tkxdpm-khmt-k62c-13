package util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FileUtil {

    public synchronized static String readJSON(String fileName) {
        String filePath = (new File("./json/" + fileName)).toString();

        StringBuilder contentBuilder = new StringBuilder();

        try (Stream<String> stream = Files.lines(Paths.get(URLDecoder.decode(filePath, "UTF-8")), StandardCharsets.UTF_8)) {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return contentBuilder.toString();
    }

    public synchronized static void writeJSON(String fileName, String jsonString) throws IOException {
        String fileNameFull = "./json/" + fileName;

        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(fileNameFull)));
        bufferedWriter.write(jsonString);
        bufferedWriter.close();
    }
}
