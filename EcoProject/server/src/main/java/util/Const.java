package util;

public class Const {
    public static final String[] ADDRESS = {
            "Hoàn Kiếm", "Đống Đa", "Ba Đình", "Hai Bà Trưng",
            "Hoàng Mai", "Thanh Xuân", "Cầu Giấy", "Hà Đông",
            "Bắc Từ Liêm", "Name Từ Liêm", "Long Biên", "Tây Hồ"};

    public static final String[] PRODUCER = {
            "Thống Nhất", "Giải Phóng", "Bắc Nam", "Hà Nội"
    };
}
