package util;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.entity.*;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static util.Const.ADDRESS;
import static util.Const.PRODUCER;

public class Seed {

    private Random random = new Random();
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    private Bike randBike() {
        String name = randString("XEDAP-", 4);
        int weight = 10 + random.nextInt(31);
        String licensePlate = randString("HN-", 6);
        String manufacturingDate = dateFormat.format(new Date(1000L * ((1288949031 + random.nextInt(315619200)))));
        String producer = PRODUCER[random.nextInt(4)];
        Bike.Status status = Bike.Status.READY;

        String cost;

        long id = System.nanoTime();

        switch (random.nextInt(3)) {
            case 0:
                cost = "5.000.000";
                return new NormalBike(id, name, weight, licensePlate, manufacturingDate, producer, cost, status);
            case 1:
                cost = "10.000.000";
                int batteryPercentage = 100;
                int loadCycles = 10000;
                int estimateUsageHourRemaining = 24;
                return new EBike(id, name, weight, licensePlate, manufacturingDate, producer, cost, status, batteryPercentage, loadCycles, estimateUsageHourRemaining);
            case 2:
                cost = "8.000.000";
                return new TwinsBike(id, name, weight, licensePlate, manufacturingDate, producer, cost, status);
            default:
                return null;
        }
    }

    private String randString(String prefix, int limit) {
        StringBuffer result = new StringBuffer();
        result.append(prefix);
        for (int i = 0; i < limit; ++i)
            result.append(random.nextInt(10));
        return result.toString();
    }

    public static void main(String[] args) throws IOException {
        Seed seed = new Seed();
        ObjectMapper objectMapper = new ObjectMapper();

        List<Parking> parkings = new ArrayList<>();

        for (int i = 0; i < ADDRESS.length; ++i) {
            int numberOfBikes = 60 + seed.random.nextInt(41);

            List<Bike> bikes = new ArrayList<>();
            Parking parking = new Parking(i + 1, ADDRESS[i], ADDRESS[i], 20, bikes);
            parkings.add(parking);

            for (int j = 0; j < numberOfBikes; ++j) {
                Bike bike = seed.randBike();
                bikes.add(bike);
            }
        }

        String json1 = objectMapper.writeValueAsString(parkings);
        FileUtil.writeJSON("parking.json", json1);
    }

}
