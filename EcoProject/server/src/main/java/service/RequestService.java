package service;

import com.fasterxml.jackson.core.JsonProcessingException;
import data.database.EcoBikeDatabase;
import data.database.IEcoBikeDatabase;
import data.entity.Request;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

@Path("/requests")
public class RequestService {

    private IEcoBikeDatabase database;

    public RequestService() throws JsonProcessingException {
        database = EcoBikeDatabase.getInstance();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Request.Result receiveRequest(Request request) throws IOException {
        return database.processRequest(request);
    }
}
