package service;

import com.fasterxml.jackson.core.JsonProcessingException;
import data.database.EcoBikeDatabase;
import data.database.IEcoBikeDatabase;
import data.entity.RentedBike;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/rented")
public class RentedBikeService {

    private IEcoBikeDatabase database;

    public RentedBikeService() throws JsonProcessingException {
        database = EcoBikeDatabase.getInstance();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<RentedBike> searchRentedBike(@QueryParam("username") String username,
                                             @QueryParam("parking_id") long parkingId,
                                             @QueryParam("card_number") long cardNumber) {
        RentedBike rentedBike = new RentedBike();
        rentedBike.setUsername(username);
        rentedBike.setParkingId(parkingId);
        rentedBike.setCardNumber(cardNumber);

        return database.searchRentedBike(rentedBike);
    }
}
