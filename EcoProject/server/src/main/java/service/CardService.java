package service;

import com.fasterxml.jackson.core.JsonProcessingException;
import data.database.EcoBikeDatabase;
import data.database.IEcoBikeDatabase;
import data.entity.CreditCard;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.List;

@Path("/cards")
public class CardService {

    private IEcoBikeDatabase database;

    public CardService() throws JsonProcessingException {
        database = EcoBikeDatabase.getInstance();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<CreditCard> searchCard(@QueryParam("user_id") long userId) throws IOException {
        CreditCard creditCard = new CreditCard();
        creditCard.setUserId(userId);
        return database.searchCard(creditCard);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public long insertCard(CreditCard card) throws IOException {
        return database.insertCard(card);
    }
}
