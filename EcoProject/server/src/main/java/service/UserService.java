package service;

import com.fasterxml.jackson.core.JsonProcessingException;
import data.database.EcoBikeDatabase;
import data.database.IEcoBikeDatabase;

import javax.ws.rs.Path;

@Path("/users")
public class UserService {

    private IEcoBikeDatabase database;

    public UserService() throws JsonProcessingException {
        database = EcoBikeDatabase.getInstance();
    }
}
