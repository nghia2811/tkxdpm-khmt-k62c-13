package service;


import com.fasterxml.jackson.core.JsonProcessingException;
import data.database.EcoBikeDatabase;
import data.database.IEcoBikeDatabase;
import data.entity.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.List;

@Path("/parkings")
public class ParkingService {

    private IEcoBikeDatabase database;

    public ParkingService() throws JsonProcessingException {
        database = EcoBikeDatabase.getInstance();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Parking> searchParking(@QueryParam("id") int id,
                                       @QueryParam("name") String name,
                                       @QueryParam("address") String address) {
        Parking parking = new Parking();
        parking.setId(id);
        parking.setName(name);
        parking.setAddress(address);
        return database.searchParking(parking);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public long insertParking(Parking parking) throws IOException {
        return database.insertParking(parking);
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public long updateParking(@PathParam("id") long id, Parking parking) throws IOException {
        return database.updateParking(id, parking);
    }

    @DELETE
    @Path("/{id}")
    public long deleteParking(@PathParam("id") long id) throws IOException {
        return database.deleteParking(id);
    }

    @GET
    @Path("/{id_parking}/bikes")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Bike> searchBike(@PathParam("id_parking") long idParking,
                                 @QueryParam("id") long id,
                                 @QueryParam("name") String name,
                                 @QueryParam("type") String type,
                                 @QueryParam("status") String status,
                                 @QueryParam("producer") String producer) {

        Bike.Type realType = Bike.Type.valueOfLabel(type);
        Bike.Status realStatus = Bike.Status.valueOfLabel(status);

        if (realType == null) {
            return database.searchBike(idParking, new Bike(id, name, 0, null, null, producer, "", realStatus, null) {
                @Override
                public long getDeposit() {
                    return 0;
                }

                @Override
                public double getRentCostMulti() {
                    return 0;
                }
            });
        } else
            switch (realType) {
                case TWINS:
                    return database.searchBike(idParking, new TwinsBike(id, name, 0, null, null, producer, null, realStatus));
                case NORMAL:
                    return database.searchBike(idParking, new NormalBike(id, name, 0, null, null, producer, null, realStatus));
                case ELECTRIC:
                    return database.searchBike(idParking, new EBike(id, name, 0, null, null, producer, null, realStatus, 0, 0, 0));
                default:
                    return null;
            }
    }


    @POST
    @Path("/{id_parking}/bikes")
    @Consumes(MediaType.APPLICATION_JSON)
    public long insertBike(@PathParam("id_parking") long idParking, Bike bike) throws IOException {
        return database.insertBike(idParking, bike);
    }

    @PUT
    @Path("/{id_parking}/bikes/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public long updateBike(@PathParam("id_parking") long idParking, @PathParam("id") long id, Bike bike) throws IOException {
        return database.updateBike(idParking, id, bike);
    }

    @DELETE
    @Path("/{id_parking}/bikes/{id}")
    public long deleteBike(@PathParam("id_parking") long idParking, @PathParam("id") long id) throws IOException {
        return database.deleteBike(idParking, id);
    }
}
