package data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("normal")
@JsonIgnoreProperties(ignoreUnknown = true)
public class NormalBike extends Bike {

    public NormalBike() {
    }

    public NormalBike(long id, String name, int weight, String licensePlate, String manufacturingDate, String producer, String cost, Status status) {
        super(id, name, weight, licensePlate, manufacturingDate, producer, cost, status, Type.NORMAL);
    }

    @Override
    public double getRentCostMulti() {
        return 1.0;
    }

    @Override
    public long getDeposit() {
        return 400000;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {" + super.toString() + "}";
    }
}
