package data.entity;

import java.util.Objects;

public class CreditCard {

    private long userId;
    private long cardNumber;
    private String owner;
    private String expirationDate = "1/1/2020";

    private long balance = 1000000;
    private String currencyUnit = "VND";

    public CreditCard() {

    }

    public CreditCard(long userId, long cardNumber, String owner, String expirationDate, long balance, String currencyUnit) {
        this.userId = userId;
        this.cardNumber = cardNumber;
        this.owner = owner;
        this.expirationDate = expirationDate;
        this.balance = balance;
        this.currencyUnit = currencyUnit;
    }



    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(long cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public String getCurrencyUnit() {
        return currencyUnit;
    }

    public void setCurrencyUnit(String currencyUnit) {
        this.currencyUnit = currencyUnit;
    }

    public boolean match(CreditCard creditCard) {
        if (creditCard.cardNumber != 0 && cardNumber != creditCard.cardNumber)
            return false;

        if (creditCard.userId != 0 && userId != creditCard.userId)
            return false;

        return true;
    }

    public boolean addMoney(long amount) {
        balance += amount;
        return true;
    }

    public boolean subtractMoney(long amount) {
        if (balance < amount)
            return false;

        balance -= amount;
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof CreditCard)
            return this.cardNumber == ((CreditCard) o).cardNumber;
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardNumber);
    }
}
