package data.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class User {
    private long id;
    private String username;
    private List<CreditCard> cards = new ArrayList<>();

    public User() {

    }

    public User(long id, String username, List<CreditCard> cards) {
        this.id = id;
        this.username = username;
        this.cards = cards;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<CreditCard> getCards() {
        return cards;
    }

    public void setCards(List<CreditCard> cards) {
        this.cards = cards;
    }

    public void update(User user) {
        id = user.id;
        username = user.username;
        cards = user.cards;
    }

    public boolean match(User user) {
        if (user == null)
            return true;

        if (user.id != 0 && this.id != user.id)
            return false;

        if (user.username != null && !user.username.equals("") && !this.username.contains(user.username))
            return false;

        return true;
    }

    public CreditCard findCard(Long cardNumber) {
        for (CreditCard c : cards) {
            if (c.getCardNumber() == cardNumber) {
                return c;
            }
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof User)
            return this.id == ((User) o).id;
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
