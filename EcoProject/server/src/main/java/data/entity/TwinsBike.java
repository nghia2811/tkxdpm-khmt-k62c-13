package data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("twins")
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwinsBike extends Bike {

    public TwinsBike() {
    }

    public TwinsBike(long id, String name, int weight, String licensePlate, String manufacturingDate, String producer, String cost, Status status) {
        super(id, name, weight, licensePlate, manufacturingDate, producer, cost, status, Type.TWINS);
    }

    @Override
    public double getRentCostMulti() {
        return 1.5;
    }

    @Override
    public long getDeposit() {
        return 550000;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {" + super.toString() + "}";
    }
}
