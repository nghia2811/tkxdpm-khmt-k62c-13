package data.entity;

import java.util.List;
import java.util.Objects;

public class Parking {
    private long id;
    private String name;
    private String address;
    private int numberOfEmptyDocks;
    private List<Bike> bikes;

    public Parking() {
    }

    public Parking(long id, String name, String address, int numberOfEmptyDocks, List<Bike> bikes) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.numberOfEmptyDocks = numberOfEmptyDocks;
        this.bikes = bikes;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getNumberOfEmptyDocks() {
        return numberOfEmptyDocks;
    }

    public void setNumberOfEmptyDocks(int numberOfEmptyDocks) {
        this.numberOfEmptyDocks = numberOfEmptyDocks;
    }

    public List<Bike> getBikes() {
        return bikes;
    }

    public void setBikes(List<Bike> bikes) {
        this.bikes = bikes;
    }

    public boolean match(Parking parking) {
        if (parking == null)
            return true;

        if (parking.id != 0 && !String.valueOf(this.id).contains(String.valueOf(parking.id)))
            return false;

        if (parking.name != null && !parking.name.equals("") && !this.name.toLowerCase().contains(parking.name.toLowerCase()))
            return false;

        if (parking.address != null && !parking.address.equals("") && !this.address.toLowerCase().contains(parking.address.toLowerCase()))
            return false;

        return true;
    }

    public void update(Parking parking) {
        name = parking.name;
        address = parking.address;
        numberOfEmptyDocks = parking.numberOfEmptyDocks;
        bikes = parking.bikes;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Parking)
            return this.id == ((Parking) o).id;
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Parking {" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
