package data.entity;

import java.util.HashMap;
import java.util.Map;

public class Request {

    private long id;
    private long userId;
    private long cardNumber;
    private long parkingId;
    private String namePark;
    private long bikeId;
    private Type type;
    private Result result;
    private Map<String, String> bundle = new HashMap<>();

    public enum Type {
        RENT, RETURN
    }

    public enum Result {
        SUCCESS, NOT_ENOUGH_MONEY, CARD_NOT_EXIST, ERROR
    }


    public String getNamePark() {
        return namePark;
    }

    public void setNamePark(String namePark) {
        this.namePark = namePark;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(long cardNumber) {
        this.cardNumber = cardNumber;
    }

    public long getParkingId() {
        return parkingId;
    }

    public void setParkingId(long parkingId) {
        this.parkingId = parkingId;
    }

    public long getBikeId() {
        return bikeId;
    }

    public void setBikeId(long bikeId) {
        this.bikeId = bikeId;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Map<String, String> getBundle() {
        return bundle;
    }

    public void setBundle(Map<String, String> bundle) {
        this.bundle = bundle;
    }
}
