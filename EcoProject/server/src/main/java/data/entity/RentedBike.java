package data.entity;

public class RentedBike {

    private Bike bike;
    private String username;
    private long parkingId;
    private String namePark;
    private long cardNumber;
    private long rentedTime;

    public String getNamePark() {
        return namePark;
    }

    public void setNamePark(String namePark) {
        this.namePark = namePark;
    }

    public Bike getBike() {
        return bike;
    }

    public void setBike(Bike bike) {
        this.bike = bike;
    }

    public long getParkingId() {
        return parkingId;
    }

    public void setParkingId(long parkingId) {
        this.parkingId = parkingId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(long cardNumber) {
        this.cardNumber = cardNumber;
    }

    public long getRentedTime() {
        return rentedTime;
    }

    public void setRentedTime(long rentedTime) {
        this.rentedTime = rentedTime;
    }

    public boolean match(RentedBike rentedBike) {
        if (rentedBike == null)
            return true;

        if (rentedBike.username != null && !rentedBike.username.equals("") && !username.contains(rentedBike.username))
            return false;

        if (rentedBike.cardNumber != 0 && !String.valueOf(cardNumber).contains(String.valueOf(rentedBike.cardNumber)))
            return false;

        if (rentedBike.parkingId != 0 && !String.valueOf(parkingId).contains(String.valueOf(rentedBike.parkingId)))
            return false;

        return true;
    }
}
