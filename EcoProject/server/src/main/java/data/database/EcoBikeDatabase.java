package data.database;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import data.entity.*;
import util.FileUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EcoBikeDatabase implements IEcoBikeDatabase {

    private volatile static EcoBikeDatabase instance;

    private ObjectMapper objectMapper;

    private List<Parking> parkings;
    private List<User> users;
    private List<RentedBike> rentedBikes;

    private EcoBikeDatabase() throws JsonProcessingException {
        objectMapper = new ObjectMapper();

        String json1 = FileUtil.readJSON("parking.json");
        parkings = objectMapper.readValue(json1, new TypeReference<List<Parking>>() {
        });

        String json2 = FileUtil.readJSON("user.json");
        users = objectMapper.readValue(json2, new TypeReference<List<User>>() {
        });

        String json3 = FileUtil.readJSON("rentedbike.json");
        rentedBikes = objectMapper.readValue(json3, new TypeReference<List<RentedBike>>() {
        });
    }

    public static EcoBikeDatabase getInstance() throws JsonProcessingException {
        if (instance == null)
            synchronized (EcoBikeDatabase.class) {
                if (instance == null)
                    instance = new EcoBikeDatabase();
            }
        return instance;
    }

    private int getNumberOfBikes() {
        int count = 0;
        for (Parking p : parkings)
            count += p.getBikes().size();

        return count;
    }

    @Override
    public List<Parking> searchParking(Parking parking) {
        List<Parking> result = new ArrayList<>();

        for (Parking p : parkings) {
            if (p.match(parking))
                result.add(p);
        }

        return result;
    }

    @Override
    public long insertParking(Parking parking) throws IOException {
        if (parking == null)
            return -1;

        if (parkings.size() == 0)
            parking.setId(1);
        else
            parking.setId(parkings.get(parkings.size() - 1).getId() + 1);

        parkings.add(parking);

        updateJSONParking();

        return parking.getId();
    }

    @Override
    public long updateParking(long id, Parking parking) throws IOException {
        try {
            for (Parking b : parkings)
                if (b.getId() == id) {
                    b.update(parking);

                    updateJSONParking();

                    return id;
                }
            return -1;
        } catch (Exception e) {
            return -1;
        }
    }

    @Override
    public long deleteParking(long id) throws IOException {
        boolean removed = parkings.removeIf(baiXe -> baiXe.getId() == id);
        if (removed) {
            updateJSONParking();
            return id;
        }
        return -1;
    }

    private Parking getParkingById(long idParking) {
        for (Parking p : parkings)
            if (p.getId() == idParking)
                return p;
        return null;
    }

    private List<Bike> getAllBike(long idParking) {
        return getParkingById(idParking).getBikes();
    }

    private Bike getBikeById(long idBike) {
        for (Parking p : parkings) {
            List<Bike> bikes = p.getBikes();
            for (Bike bike : bikes)
                if (bike.getId() == idBike)
                    return bike;
        }
        return null;
    }

    @Override
    public List<Bike> searchBike(long idParking, Bike bike) {
        List<Bike> bikes = getAllBike(idParking);

        List<Bike> result = new ArrayList<>();

        for (Bike b : bikes)
            if (b.match(bike))
                result.add(b);


        return result;
    }

    @Override
    public long insertBike(long idParking, Bike bike) throws IOException {
        if (bike == null)
            return -1;

        List<Bike> bikes = getAllBike(idParking);

        bike.setId(System.nanoTime());
        bikes.add(bike);

        updateJSONParking();

        return bike.getId();
    }

    @Override
    public long updateBike(long idParking, long id, Bike bike) throws IOException {
        try {
            List<Bike> bikes = getAllBike(idParking);

            for (Bike b : bikes)
                if (b.getId() == id) {
                    b.update(bike);

                    updateJSONParking();

                    return id;
                }

            return -1;
        } catch (Exception e) {
            return -1;
        }

    }

    @Override
    public long deleteBike(long idParking, long id) throws IOException {
        List<Bike> bikes = getAllBike(idParking);

        boolean removed = bikes.removeIf(bike -> bike.getId() == id);

        if (removed) {
            updateJSONParking();
            return id;
        }

        return -1;
    }

    private User getUserById(long id) {
        for (User user : users)
            if (user.getId() == id)
                return user;
        return null;
    }

    @Override
    public List<User> searchUser(User user) {
        List<User> result = new ArrayList<>();

        for (User u : users)
            if (u.match(user))
                result.add(u);

        return result;
    }

    @Override
    public List<RentedBike> searchRentedBike(RentedBike rentedBike) {
        List<RentedBike> result = new ArrayList<>();
        for (RentedBike b : rentedBikes)
            if (b.match(rentedBike))
                result.add(b);

        return result;
    }

    public List<CreditCard> searchCard(CreditCard card) throws IOException {
        List<CreditCard> result = new ArrayList<>();

        for (User user : users)
            for (CreditCard creditCard : user.getCards())
                if (creditCard.match(card))
                    result.add(creditCard);

        return result;
    }

    @Override
    public long insertCard(CreditCard card) throws IOException {
        User user = new User();
        user.setId(card.getUserId());

        List<User> temp = searchUser(user);

        User user2 = temp.get(0);

        for (CreditCard creditCard : user2.getCards())
            if (creditCard.getCardNumber() == card.getCardNumber())
                return -1;

        temp.get(0).getCards().add(card);
        updateJSONUser();

        return card.getCardNumber();
    }

    @Override
    public Request.Result processRequest(Request request) throws IOException {
        Request.Result response = Request.Result.ERROR;

        Bike bike = getBikeById(request.getBikeId());
        User user = getUserById(request.getUserId());
        Parking current = getParkingById(request.getParkingId());
        request.setNamePark(current.getName());

        CreditCard card = user.findCard(request.getCardNumber());

        if (card == null) {
            response = Request.Result.CARD_NOT_EXIST;
            return response;
        }


        switch (request.getType()) {
            case RENT:
                boolean res1 = card.subtractMoney(Long.parseLong(request.getBundle().get("deposit")));

                if (res1) {
                    bike.setStatus(Bike.Status.RENTED);

                    User temp = new User();
                    temp.setId(request.getUserId());

                    String username = searchUser(temp).get(0).getUsername();

                    RentedBike rentedBike = new RentedBike();
                    rentedBike.setRentedTime(System.currentTimeMillis());
                    rentedBike.setBike(bike);
                    rentedBike.setUsername(username);
                    rentedBike.setCardNumber(request.getCardNumber());
                    rentedBike.setParkingId(request.getParkingId());
                    rentedBike.setNamePark(request.getNamePark());
                    rentedBikes.add(rentedBike);

                    current.setNumberOfEmptyDocks(current.getNumberOfEmptyDocks() + 1);
                    response = Request.Result.SUCCESS;
                } else {
                    response = Request.Result.NOT_ENOUGH_MONEY;
                }
                break;
            case RETURN:
                boolean res2 = card.addMoney(Long.parseLong(request.getBundle().get("deposit")));
                boolean res3 = card.subtractMoney(Long.parseLong(request.getBundle().get("cost")));

                if (res3) {
                    bike.setStatus(Bike.Status.READY);

                    long previousParkingId = Long.parseLong(request.getBundle().get("previous_parking"));

                    Parking temp = getParkingById(previousParkingId);
                    temp.getBikes().removeIf(b -> b.getId() == bike.getId());

                    current.setNumberOfEmptyDocks(current.getNumberOfEmptyDocks() - 1);
                    current.getBikes().add(bike);

                    rentedBikes.removeIf(rentedBike -> rentedBike.getBike().getId() == request.getBikeId());

                    response = Request.Result.SUCCESS;

                } else {
                    card.subtractMoney(Long.parseLong(request.getBundle().get("deposit")));
                    response = Request.Result.NOT_ENOUGH_MONEY;
                }
                break;
        }

        request.setResult(response);
        updateJSONUser();
        updateJSONParking();
        updateRented();

        return response;
    }

    public void updateJSONParking() throws IOException {
        String json = objectMapper.writeValueAsString(parkings);
        FileUtil.writeJSON("parking.json", json);
    }

    public void updateJSONUser() throws IOException {
        String json = objectMapper.writerFor(new TypeReference<List<User>>() {
        }).writeValueAsString(users);
        FileUtil.writeJSON("user.json", json);
    }

    public void updateRented() throws IOException {
        String json = objectMapper.writerFor(new TypeReference<List<RentedBike>>() {
        }).writeValueAsString(rentedBikes);
        FileUtil.writeJSON("rentedbike.json", json);
    }
}
