package data.database;

import data.entity.*;

import java.io.IOException;
import java.util.List;

public interface IEcoBikeDatabase {

    List<Parking> searchParking(Parking parking);

    long insertParking(Parking parking) throws IOException;

    long updateParking(long id, Parking parking) throws IOException;

    long deleteParking(long id) throws IOException;

    List<Bike> searchBike(long idParking, Bike bike);

    long insertBike(long idParking, Bike bike) throws IOException;

    long updateBike(long idParking, long id, Bike bike) throws IOException;

    long deleteBike(long idParking, long id) throws IOException;

    List<User> searchUser(User user);


    public List<RentedBike> searchRentedBike(RentedBike rentedBike);

    long insertCard(CreditCard card) throws IOException;

    List<CreditCard> searchCard(CreditCard card) throws IOException;

    Request.Result processRequest(Request request) throws IOException;
}
